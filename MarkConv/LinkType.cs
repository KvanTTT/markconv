﻿namespace MarkConv
{
    public enum LinkType
    {
        Absolute,
        Relative,
        Local
    }
}
