﻿namespace MarkConv
{
    public interface ILogger
    {
        void Warn(string message);

        void Info(string message);
    }
}
