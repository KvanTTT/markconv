﻿namespace MarkConv
{
    public enum ElementType
    {
        Link,
        DetailsElement,
        SummaryElements,
        SpoilerOpenElement,
        SpoilerCloseElement,
        AnchorElement,
        HtmlLink
    }
}
